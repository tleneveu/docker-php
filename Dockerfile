FROM php:8.2-cli

RUN docker-php-ext-install opcache pdo_mysql

RUN pecl install xdebug-3.2.0

RUN docker-php-ext-enable xdebug

WORKDIR /var/www

CMD ["php", "-S", "0.0.0.0:8080", "-t", "public/"]

EXPOSE 8000